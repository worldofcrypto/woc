const { Connection, LAMPORTS_PER_SOL, Keypair } = require('@solana/web3.js');
const { Contract, publicKeyToHex } = require('@solana/solidity');
const { readFileSync, writeFileSync } = require('fs');

const ERC20_ABI = JSON.parse(readFileSync('./build/ERC20.abi', 'utf8'));
const BUNDLE_SO = readFileSync('./build/bundle.so');

(async function () {
    console.log('Connecting to your dev solana node ...');
    const connection = new Connection('https://api.devnet.solana.com', 'confirmed');

    key = Uint8Array.from([207,6,195,152,201,169,228,252,17,180,195,82,252,70,126,238,164,167,162,235,91,18,68,193,52,69,65,74,63,179,241,94,97,173,232,173,139,159,19,26,58,114,236,193,254,96,52,44,193,159,156,160,30,152,84,235,214,235,55,173,33,150,127,186]);
    const payer = Keypair.fromSecretKey(key);
    console.log(payer.publicKey.toBase58());
    /*
    while (true) {
        console.log('Airdropping SOL to a new wallet ...');
        await connection.requestAirdrop(payer.publicKey, 1 * LAMPORTS_PER_SOL);
        await new Promise((resolve) => setTimeout(resolve, 1000));
        if (await connection.getBalance(payer.publicKey)) break;
    }
    */

    const address = publicKeyToHex(payer.publicKey);
    const program = Keypair.generate();
    const storage = Keypair.generate();

    writeFileSync('../config/program.json', '[' + program.secretKey.toString() + ']');
    writeFileSync('../config/storage.json', '[' + storage.secretKey.toString() + ']');


    const contract = new Contract(
        connection,
        program.publicKey,
        storage.publicKey,
        ERC20_ABI,
        payer
    );

    console.log('Deploying the Solang-compiled ERC20 program ...');
    await contract.load(program, BUNDLE_SO);

    console.log('Program deployment finished, deploying the ERC20 contract ...');
    await contract.deploy(
        'ERC20',
        ['World Of Crypto', 'wocGold', '1000000000000000000'],
        program,
        storage,
        4096 * 8
    );

    console.log('Contract deployment finished, invoking some contract functions ...');
    const symbol = await contract.symbol();
    const balance = await contract.balanceOf(address);

    console.log(`ERC20 contract for ${symbol} deployed!`);
    console.log(`Your wallet at ${address} has a balance of ${balance} tokens.`);

    console.log(`Program address: ${program.publicKey.toBase58()}`);
    console.log(`Storage Address: ${storage.publicKey.toBase58()}`);



    //contract.addEventListener(function (event) {
    //    console.log(`${event.name} event emitted!`);
    //    console.log(`${event.args[0]} sent ${event.args[2]} tokens to ${event.args[1]}`);
    //});

    //console.log('Sending tokens will emit a "Transfer" event ...');
    //const recipient = Keypair.generate();
    //await contract.transfer(publicKeyToHex(recipient.publicKey), '1000000000000000000');



    process.exit(0);
})();
